# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.2.4] - [2023-03-30]


- see  #24727 upgrade version just for removing the gcube-staging dependencies 

